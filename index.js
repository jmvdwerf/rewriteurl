
var buttons = require('sdk/ui/button/action');
var tabs = require("sdk/tabs");
var urls = require('sdk/url');
var config = require('sdk/simple-prefs') 

var button = buttons.ActionButton({
  id: "mozilla-link",
  label: "Visit Mozilla",
  icon: {
    "16": "./icon-16.png",
    "32": "./icon-32.png",
    "64": "./icon-64.png"
  },
  onClick: handleClick
});

function handleClick(state) {
	var url = require("sdk/url").URL(tabs.activeTab.url);
	if (url.host) {
		var prefix = config.prefs['prefix'];
		if (prefix != '') {
			prefix += ".";
		}
		var postfix =config.prefs['postfix'];
		if (postfix != '') {
			postfix = "." + postfix;
		}
		
		newUrl = tabs.activeTab.url.replace(url.host, prefix + url.host + postfix);
		console.log("Rewrite to: " + newUrl);
		tabs.activeTab.url = newUrl;
	}
}